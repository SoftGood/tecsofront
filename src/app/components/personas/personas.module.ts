import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasComponent } from './personas.component';
import { PeticionesService } from 'src/app/services/peticiones.service';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from "angular2-materialize";

@NgModule({
  declarations: [PersonasComponent],
  imports: [
    CommonModule,
     MaterializeModule,
    PersonasRoutingModule,

    FormsModule
  ],
    providers: [PeticionesService],
})
export class PersonasModule { }
