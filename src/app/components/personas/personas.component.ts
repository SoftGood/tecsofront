import { Component, OnInit } from '@angular/core';
import { PeticionesService } from 'src/app/services/peticiones.service';
import Swal from 'sweetalert2';
import { FormGroup } from '@angular/forms';
declare var Materialize: any;
declare var $: any;
@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {
 private fechaActual = new Date().getTime();
  public dataObject = {
    rut: '',
    nombre: '',
    apellidos: '',
    razonsocial: '',
    aniofundacion: '',
    fechamodificacion: this.fechaActual,    
    fechacreacion: this.fechaActual,
    identificacion: ''
  };
  public listarPersonas = [];

  public tipoPersona = [
    {
      codigo: 1,
      descripcion: 'Fisica'
    },
    {
      codigo: 2,
      descripcion: 'Juridica'
    }
  ];
  isModificar: boolean;
  indexSelect: any;

  public tipoPersonaModel: any;
  
  constructor(public services: PeticionesService) { }

  public obtenerPersona(): void {
    this.services.obtenerPersonas().subscribe((response: any) => {
      if (response.exitoso) {
        this.listarPersonas = response.data;
      } else {
         this.mensja(
        'Información',
        response.mensaje);
      }
    }, () => {
        this.mensja(
          'Advertencia',
          "No se pudo establecer conexión con el servidor"
        );
    });
  }

  public eliminar(eliminar: any, posicion: any): void {
    this.services.eliminarPersona(eliminar)
      .subscribe((response: any) => {
          this.mensja(
        'Información',
            response.mensaje);
        
        this.listarPersonas.splice(posicion, 1);
      }, () => {
          this.mensja(
            'Advertencia',
            "No se pudo establecer conexión con el servidor"
          );
      });
  
  }


  public irEditar(data: any, index): void {

    if (index !== this.indexSelect) {
      $('html, body').animate({ scrollTop: "250px" }, 800);
    }
    if (data.razonsocial) {
      this.tipoPersonaModel = 2;
    } else {
      this.tipoPersonaModel = 1;
    }
    this.indexSelect = index;
    this.dataObject = data;
    this.isModificar = true;

    setTimeout(() => {
          Materialize.updateTextFields();
    }, 100);

    console.log(data);
  }



  public modificarPersona(): void {
    this.services.modificarPersona(this.dataObject)
      .subscribe((response: any) => {

      if (response.exitoso) {
        this.mensja(
          'Información',
           response.mensaje,
          'success'
        );
    
        if (this.indexSelect) {
          this.listarPersonas[this.indexSelect] = this.dataObject;
          this.limpiar();
        } else {
          location.reload();
        }

      } else {
         this.mensja(
        'Información',
        response.mensaje);
      }
    }, () => {
          this.mensja(
          'Advertencia',
          "No se pudo establecer conexión con el servidor"
        );
    });
  }
  
  public crarPersona(): void {
    this.services.agregarPersona(this.dataObject)
      .subscribe((response: any) => {
      console.log('data --> ', response);
      if (response.exitoso) {
        this.mensja(
          'Información',
            response.mensaje,
          'success'
        );
        this.listarPersonas.push(this.dataObject);
        this.limpiar();
      } else {
         this.mensja(
        'Información',
        response.mensaje);
      }
    }, () => {
          this.mensja(
          'Advertencia',
          "No se pudo establecer conexión con el servidor"
        );
    });
  }


  private mensja(titulo: string, mensaje: string, tipo?:any) {
    Swal.fire(
      titulo,
       mensaje,
       tipo
    );
  }

  public vaidaCampo(): boolean {
    return null;
  }

 public limpiar(opcion?: any): void{
    this.dataObject = {
      rut: '',
      nombre: '',
      apellidos: '',
      razonsocial: '',
      aniofundacion: '',
      fechamodificacion: this.fechaActual,    
      fechacreacion: this.fechaActual,
      identificacion: ''
    };
    this.indexSelect = '';
      this.isModificar = false;
      
      if (opcion) {
        this.tipoPersonaModel = '';
      }
  }


  ngOnInit() {
    this.obtenerPersona();
  }

}
