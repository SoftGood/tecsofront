import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PeticionesService {
baseUrl = 'localhost:8080';
  header: HttpHeaders;
  constructor(private http: HttpClient) { 
    this.header = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    })
  }

  obtenerPersonas(): Observable<Object> {
    return this.http
      .get(
        `http://${this.baseUrl}/personas/obtenerPersonas`, 
        {headers: this.header}
      )
      .pipe(map((response) => response, error => error));
  }

  agregarPersona(parametros: any): Observable<Object> {
    return this.http
      .post(
        `http://${this.baseUrl}/personas/crearPersona`,
        parametros,  {headers: this.header}
      )
      .pipe(map((response) => response, error => error));
  }


   eliminarPersona(parametros: any): Observable<Object> {
    return this.http
      .post(
        `http://${this.baseUrl}/personas/eliminarPersona`,
        parametros,  {headers: this.header}
      )
      .pipe(map((response) => response, error => error));
  }


   modificarPersona(parametros: any): Observable<Object> {
    return this.http
      .post(
        `http://${this.baseUrl}/personas/editarPersona`,
        parametros,  {headers: this.header}
      )
      .pipe(map((response) => response, error => error));
  }



}

